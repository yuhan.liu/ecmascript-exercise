async function fetchData(url) {
  // <-- start
  try {
    const result = await fetch(url);
    const data = await result.json();
    console.log(data);
  } catch (error) {
    console.error(error);
  }
  // end -->
}

const URL = 'http://localhost:3000/api';
fetchData(URL);
