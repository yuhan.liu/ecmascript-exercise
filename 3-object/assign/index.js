export default function addSerialNumber(source) {
  return Object.assign(
    source,
    { serialNumber: '12345' },
    { type: 'accessory' },
    {
      properties: {
        color: 'green',
        status: 'processed'
      }
    }
  );
}
