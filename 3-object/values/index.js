export default function countTypesNumber(source) {
  return Object.keys(source).reduce((sum, e) => sum + Number(source[e]), 0);
}
