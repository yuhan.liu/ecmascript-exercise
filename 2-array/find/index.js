export default function find00OldPerson(collection) {
  return collection.find(e => e.age < 20 && e.age > 9).name;
}
