export default function numberMapToWord(collection) {
  return collection.map(e => String.fromCharCode(96 + e));
}
